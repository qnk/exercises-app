# Exercises App

>Exercises App, by José Antonio Cuenca, **fullstack JavaScript developer** and **fourth-sector entrepreneur** based on Barcelona. Check my [Linkedin profile](www.linkedin.com/in/josé-antonio-cuenca-9b085935)

Exercises App it's an application for training purposes. It integrates:
- Angular material
- Flexbox layout

The project follows [Use Angular, Angular Material, Angularfire and NgRx to build a real Angular App course](https://www.udemy.com/angular-full-app-with-angular-material-angularfire-ngrx/learn/v4/overview) by 
[Maximilian Schwarzmüller](https://www.udemy.com/user/maximilian-schwarzmuller/) released on 2018.

## How to use

- Login or signup with any valid email and password.
- Select a training exercise and start it.
- Stop or let the countdown end.
- Check every exercise (completed or cancelled) from Past trainings tab in trainings view.

## Roadmap

**Exercises App** is still under deployment. These are the next features to be released:
- Many code improvements such as error management scaffold
- Deploy sidebar
- Integration with Firebase
- Include tests

## License

Due to this project has been done for my personal training purposes, I've followed [Maximilian Schwarzmüller's](https://www.udemy.com/user/maximilian-schwarzmuller/) tutorial and he owns the license.